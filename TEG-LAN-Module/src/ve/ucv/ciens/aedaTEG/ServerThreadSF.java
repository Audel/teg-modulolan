package ve.ucv.ciens.aedaTEG;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class ServerThreadSF extends Thread
{
   private Socket server;
   public String pathArchivoTrans;
   
   public ServerThreadSF(Socket socket, String filePath) throws IOException
   {
      this.server = socket;
      this.pathArchivoTrans = filePath;
   }

   public void run()
   {
         try
         {
           System.out.println("Conectado a " + server.getRemoteSocketAddress());
            DataInputStream in = new DataInputStream(server.getInputStream());
            //System.out.println(in.readUTF());
            
            //EL ARCHIVO QUE SE VA A ENVIAR
            byte[] buffer_s = new byte[1024];
            //LECTURA
            DataOutputStream out = new DataOutputStream(server.getOutputStream());
            out.writeUTF("FILETRANS|graf.jpg");
            
            try {
                //File file = new File("/mnt/sdcard/", "bluetooth.png");
            	File file = new File(pathArchivoTrans);
                InputStream is = new FileInputStream(file);
                int read = 0;
                String ver;
                try {
                    while((read = is.read(buffer_s)) != -1){   
                        out.write(buffer_s, 0, read);
                        out.flush();
                    }
                    //Toast.makeText(getApplicationContext(), "TERMINE DE ENVIAR", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                //por ultimo despues denviar cierra el archivo y lo borra
                is.close();
               
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            //cierra
            out.close();
            server.close();
         }catch(SocketTimeoutException s)
         {
            System.out.println("Socket timed out!");
         }catch(IOException e)
         {
            e.printStackTrace();
         }
   }
   
}
