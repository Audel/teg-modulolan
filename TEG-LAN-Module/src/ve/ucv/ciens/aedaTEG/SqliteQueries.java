package ve.ucv.ciens.aedaTEG;
import java.io.PrintWriter;
import java.sql.*;
import java.text.SimpleDateFormat;

public class SqliteQueries {
	
	public SqliteQueries() {
		// TODO Auto-generated constructor stub
	}

	public Boolean UsuarioExiste(String user, String password) {                                         
		// VARIABLES
		Boolean exito = false;
		
		Connection c = null;
		Statement stmt = null;
		PreparedStatement pst = null;
		ResultSet resultSet = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Audel\\Desktop\\data_destruible.db3");
			//c = DriverManager.getConnection("C:/Users/Audel/Desktop/data_destruible.db3");
			System.out.println("Opened database successfully");

			String consLoginSql = "SELECT * FROM user WHERE username=? AND password=?";
			try{
				pst = c.prepareStatement(consLoginSql);
				pst.setString(1, user);
				pst.setString(2, password);

				resultSet=pst.executeQuery();
				if(resultSet.next()){
					System.out.println("login correcto");
					exito = true;
				}else{
					System.out.println("login INcorrecto");
				}

			}catch(Exception e){
				System.out.println(e);
			}

			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Consulta exitosa");
		
		return(exito);
	}
	
	public Boolean PacienteSelExiste(String pacienteId) {                                         
		// VARIABLES
		Boolean exito = false;
		
		Connection c = null;
		Statement stmt = null;
		PreparedStatement pst = null;
		ResultSet resultSet = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Audel\\Desktop\\data_destruible.db3");
			//c = DriverManager.getConnection("C:/Users/Audel/Desktop/data_destruible.db3");
			System.out.println("Opened database successfully");

			String consLoginSql = "SELECT * FROM patient WHERE user_id=? AND deleted='False' ";
			try{
				pst = c.prepareStatement(consLoginSql);
				pst.setString(1, pacienteId);

				resultSet=pst.executeQuery();
				if(resultSet.next()){
					System.out.println("paciente existe");
					exito = true;
				}else{
					System.out.println("paciente no existe");
				}
				
				if(exito){
					//AGARRO EL NOMBRE DE PACIENTE Y APELLIDO QUE DEBERIA DEVOLVER
					resultSet.getString("user_id");
					resultSet.getString("names");
					resultSet.getString("lastnames");
				}
				

			}catch(Exception e){
				System.out.println(e);
			}

			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Consulta exitosa");
		
		return(exito);
	}
	
	public Boolean InsertarDatosJuego(String pacienteId, String fecha, String idGame, String datos) {                                         
		// VARIABLES
		Boolean exito = false;
		
		Connection c = null;
		Statement stmt = null;
		PreparedStatement pst = null;
		ResultSet resultSet = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Audel\\Desktop\\data_destruible.db3");
			//c = DriverManager.getConnection("C:/Users/Audel/Desktop/data_destruible.db3");
			System.out.println("Opened database successfully");

			PreparedStatement statement = c.prepareStatement(
					"INSERT INTO gametable (idPatient,idGame,date,dataGame,hand)VALUES(?,?,?,?,?)");
			
			statement.setInt(1, Integer.valueOf(pacienteId));
			statement.setInt(2, Integer.valueOf(idGame));
			//statement.setDate(3, (Date) new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fecha));
			statement.setString(3,fecha);
			statement.setString(4,datos);
			statement.setString(5, "1");
			int exe = statement.executeUpdate();
			
			if(exe != 0){
				exito=true;
				System.out.println("Consulta para ingresar a la BD exitosa");
			}

			// IMPORTANTE HACER COMMIT DE LOS DATOS
			//c.commit();
			//cierra la conexion
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Consulta exitosa");
		
		return(exito);
	}
	
	public String GenerarArchivoParaGrafs(String idPaciente, String datos){
		//primero organizamos unas variables
		String [] datosSeparados = datos.split("\\^");
		String pathArchivoSalida = "C:\\Users\\Audel\\Desktop\\salidatemporal.txt";
		
		//lueo se crean los Strings que determinan la grafica final en base a los datos solicitados
		Boolean soloUltJuego = false;
		Boolean ultiFechaNotSet = true;
		Date ultimaFecha = new Date(2006, 12, 31);
		String lineaEjeX = "";
		String lineaEjeY = "";
		String nombrePeriodo = "";
		String tokenBusquedaEst = "";
		if(datosSeparados[2].equals("1")){
			soloUltJuego = true;
			nombrePeriodo = "(Ultima Sesion) ";
			lineaEjeX = "Valores ultima Sesion de Juego";
		}else if(datosSeparados[2].equals("0")){
			nombrePeriodo = "(Historico de Juegos) ";
			lineaEjeX = "Fechas Sesiones de Juego";
		}
		String nombreJuego = "";
		switch(Integer.valueOf(datosSeparados[1])){
		case 0:
			nombreJuego = "Puzzle ";
			break;
		case 1:
			nombreJuego = "Viste al Personaje ";
			break;
		case 2:
			nombreJuego = "Fabrica ";
			break;
		case 3:
			nombreJuego = "Pintar y Rellenar ";
			break;
		default:
			nombreJuego = "Puzzle ";
			break;
		}		
		String nombreDato = "";
		switch(Integer.valueOf(datosSeparados[0])){
		case 0:
			nombreDato = "Tiempo de Juego ";
			lineaEjeY = "Tiempo de Juego (mins)";
			tokenBusquedaEst = "tiempo";
			break;
		case 1:
			nombreDato = "Separaci�n de Dedos ";
			lineaEjeY = "Separaci�n de Dedos (cm)";
			//este dato se refiere al promedio de separacion de dedos
			tokenBusquedaEst = "promedios";
			break;
		case 2:
			nombreDato = "Porcentaje de Relleno ";
			lineaEjeY = "Procentaje de Area Rellenada";
			//buscara por pronedio de porcentaje relleno de fira
			tokenBusquedaEst = "promedios";
			break;
		default:
			nombreDato = "Tiempo de Juego ";
			lineaEjeY = "Tiempo de Juego (mins)";
			tokenBusquedaEst = "tiempo";
			break;
		}
		//El codiggo de los juegos en la base de datos va desde 1001 por lo cual se le suma es cantidad
		int gameCode = Integer.valueOf(datosSeparados[1]) + 1001;
		
		//EL PROCESO DE CONSULTAR A LA BD
		Connection c = null;
		Statement stmt = null;
		PreparedStatement pst = null;
		ResultSet resultSet = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Audel\\Desktop\\data_destruible.db3");
			//c = DriverManager.getConnection("C:/Users/Audel/Desktop/data_destruible.db3");
			System.out.println("Opened database successfully");

			String consLoginSql = "SELECT * FROM gameTable WHERE idPatient=? AND idGame=? ORDER BY date DESC";
			try{
				//LA "�MPRESION" EN EL ARCHIVO
                PrintWriter writer = new PrintWriter(pathArchivoSalida, "UTF-8");
                if(soloUltJuego){
                    writer.println("barra");
                }else{
                    writer.println("serie");
                }
                //ESCRIBE EL TITULO DE LA GRAFICA
                writer.println(nombreDato + nombreJuego + nombrePeriodo);
                writer.println(lineaEjeX);
                writer.println(lineaEjeY);
				
				pst = c.prepareStatement(consLoginSql);
				pst.setString(1, idPaciente);
				pst.setString(2, String.valueOf(gameCode));

				resultSet=pst.executeQuery();
				if(resultSet.next()){
					System.out.println("hay registros para el juego");

					//intenta volver a hacer el query para ahora si guardar
					resultSet=pst.executeQuery();

					System.out.println("Encontrados algunos juegos");
					while(resultSet.next()){
						String lineaRow = resultSet.getString("dataGame");
						System.out.println(lineaRow);
						//this.setVisible(false);
						Date fechaSel = resultSet.getDate("date");
						String fechaSelStr = resultSet.getString("date");

						String [] fechaSelStrSplit = fechaSelStr.split("-");
						fechaSel = new Date(Integer.parseInt(fechaSelStrSplit[0]), 
								Integer.parseInt(fechaSelStrSplit[1]), 
								Integer.parseInt(fechaSelStrSplit[2].split(" ")[0]) );
						System.out.println(fechaSel);
						if(soloUltJuego){
							//guarda unicamente los datos cuya fecha sea igual a la primera
							//uuso el ooolean para el primer caso en qe no se cuual es la ultima fecha
							if(ultiFechaNotSet){
								//ultimaFecha = resultSet.getDate("date");
								ultimaFecha = fechaSel;
								ultiFechaNotSet = false;
								String [] lineaDataRow = resultSet.getString("dataGame").split("\\^");
								for(int ild=0; ild<lineaDataRow.length; ild++){
									System.out.println("un pedacito es " + lineaDataRow[ild]);
									String [] undato = lineaDataRow[ild].split("=");
									if(undato[0].equalsIgnoreCase(tokenBusquedaEst)){
										//encontro el dato actual de la busqueda y lo mete en el archivo
										writer.println(fechaSelStr + " " + undato[1]);
									}
								}
								//FIN DEL FOR
							}else{
								//compara todas las siguientes fechas contra la priemra para obtener unicamente la ultima sesion
								if(ultimaFecha.compareTo(fechaSel) == 0 ){
									//la fecha actual es igual a la ultima fehca asi que se guarda
									System.out.println("se encontro una fecha igual a la ultima");
									System.out.println(fechaSel);
									String [] lineaDataRow = resultSet.getString("dataGame").split("\\^");
									for(int ild=0; ild<lineaDataRow.length; ild++){
										System.out.println("un pedacito es " + lineaDataRow[ild]);
										String [] undato = lineaDataRow[ild].split("=");
										if(undato[0].equalsIgnoreCase(tokenBusquedaEst)){
											//encontro el dato actual de la busqueda y lo mete en el archivo
											writer.println(fechaSelStr + " " + undato[1]);
										}
									}
									//YAY CASO FASTIDIOSO LISTO
								}
							}
						}else{
							//simplemente coloca todas las fechas
							System.out.println("una fecha del historico");
							System.out.println(fechaSel);
							String [] lineaDataRow = resultSet.getString("dataGame").split("\\^");
							for(int ild=0; ild<lineaDataRow.length; ild++){
								String [] undato = lineaDataRow[ild].split("=");
								if(undato[0].equalsIgnoreCase(tokenBusquedaEst)){
									//encontro el dato actual de la busqueda y lo mete en el archivo
									writer.println(fechaSelStr + " " + undato[1]);
								}
							}
						}
					}

					//writer.println("The first line");
					//writer.println("The second line");
					writer.close();
					
				}else{
					System.out.println("no hay registros para el juego");
				}
				
				/*if(exito){
					//AGARRO EL NOMBRE DE PACIENTE Y APELLIDO QUE DEBERIA DEVOLVER
					resultSet.getString("user_id");
					resultSet.getString("names");
					resultSet.getString("lastnames");
				}*/
				
			}catch(Exception e){
				System.out.println(e);
			}

			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Consulta terminada");
		return pathArchivoSalida;
	}
	
}
