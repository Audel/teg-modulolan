package ve.ucv.ciens.aedaTEG;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class ServerModule {

	/**
	 * @param args
	 */
	//objeto de la clase sqlitequeries para hacer consultas a BD
	static SqliteQueries consultador;
	
	//Server object
	static Server server;
	//Ports to listen on
	static int udpPort = 27967, tcpPort = 27967;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Comienza inicializacion del servidor");
		
		//Create the server
		server = new Server();
		
		//Register a packet class.
		server.getKryo().register(PacketMessage.class);
		//We can only send objects as packets if they are registered.
		
		//Bind to a port
		try {
			server.bind(tcpPort, udpPort);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
		
		//Start the server
		server.start();
		
		//tambien inicializo el objjeto de la sqlitequeries para hacer consultas
		consultador = new SqliteQueries();
		
		//Add the listener
		server.addListener(new Listener(){
			
			public void connected(Connection c){
				System.out.println("Received a connection from "+c.getRemoteAddressTCP().getHostString());
				//Create a message packet.
				PacketMessage packetMessage = new PacketMessage();
				//Assign the message text.
				packetMessage.message = "Hello friend! The time is: "+new Date().toString();
				
				//Send the message
				c.sendTCP(packetMessage);
				//Alternatively, we could do:
				//c.sendUDP(packetMessage);
				//To send over UDP.
			} 
			
			public void received (Connection connection, Object object) {
				if (object instanceof PacketMessage) {
					PacketMessage request = (PacketMessage) object;
					System.out.println(request.message);
					
					if(request.message.contains("|")){
						System.out.println("mensaje parte del protocolo");
						String[] msgPicado = request.message.split("\\|");
						if(msgPicado[0].equalsIgnoreCase("login")){
							System.out.println("deberia verificar si exist en bd");
							if(consultador.UsuarioExiste(msgPicado[1], msgPicado[2])){
								//EL LOGIN ES EXITOSO y debe contestarle con la respuesta adecuada
								System.out.println("login exitoso");
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "LOGINR|OK";
								connection.sendTCP(lResponse);
							}else{
								System.out.println("login FALLIDO");
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "LOGINR|FAIL";
								connection.sendTCP(lResponse);
							}
						}
						if(msgPicado[0].equalsIgnoreCase("patientsearch")){
							if(consultador.PacienteSelExiste(msgPicado[1])){
								System.out.println("El paciente indicado fue encontrado");
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "PATIENTSEARCHR|OK";
								connection.sendTCP(lResponse);
							}else{
								System.out.println("El paciente indicado no existe");
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "PATIENTSEARCHR|FAIL";
								connection.sendTCP(lResponse);
							}
						}
						if(msgPicado[0].equalsIgnoreCase("addgamedata")){
							if(consultador.InsertarDatosJuego(msgPicado[1], msgPicado[2], msgPicado[3], msgPicado[4]) ){
								System.out.println("Inresados Los Datos en BD");
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "ADDGAMEDATAR|OK";
								connection.sendTCP(lResponse);
							}else{
								System.out.println("El paciente indicado no existe");
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "ADDGAMEDATAR|FAIL";
								connection.sendTCP(lResponse);
							}
						}
						if(msgPicado[0].equalsIgnoreCase("getgraf")){
							//SE REQUIERE UNA GRAFICA
							//Primero se hace la cosnulta a la BD y se genera el archivo de texto con los datos especificados
							String pathArchivoSalida = consultador.GenerarArchivoParaGrafs(msgPicado[1], msgPicado[2]);
							
							//UNA VEZ HAYA REALIZADO EL QUERY DEBE EJECUTAR EL JAR QUE GENERA LA GRAFICA
							ProcessBuilder pb = new ProcessBuilder("java", "-jar", "prueba.jar", pathArchivoSalida);
							pb.redirectErrorStream(true);
							pb.directory(new File("C:\\Users\\Audel\\Desktop"));
							//booleano para saber si carga la imamgen
							Boolean succesImage = false;

							System.out.println("Directory: " + pb.directory().getAbsolutePath());
							Process p;
							String pathline = new String();
							try {
								p = pb.start();

								InputStream is = p.getInputStream();
								BufferedReader br = new BufferedReader(new InputStreamReader(is));
								for (String line = br.readLine(); line != null; line = br.readLine()) {
									System.out.println( line ); // Or just ignore it
									//verifica si hay la linea scces
									if(line.equalsIgnoreCase("SUCCESS")){
										succesImage = true;
										pathline = br.readLine();
									}
								}
							} catch (IOException ex) {
								System.out.println("Error grave leyendo salida de proceso: " + ex);
							}
							System.out.println("finalizo la ejecucion y debe enviar la imagen");

							if(succesImage){
								//INMEDIATAMENTE SE HACE EL ENVIO DEL ARCHIVO POR VIA DE SOCEKTS EN UN HILO
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "REQR|OK";
								connection.sendTCP(lResponse);
								//PORQUE PUEDO
								try
								{
									ServerSocket serverSocket;
									serverSocket = new ServerSocket(tcpPort+1);
									serverSocket.setSoTimeout(10000); 
									System.out.println("Esperando por Conexion del cliente en puerto " +
								            serverSocket.getLocalPort() + "...");
						            Socket server = serverSocket.accept();					            
						            serverSocket.close();
						            System.out.println("iniciando hilo de servidor para enviar archivo...");
						            //Thread t = new ServerThreadSF(server, pathline);
						            //QUICKFIX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!++++++++++++++
						            Thread t = new ServerThreadSF(server, pb.directory().getAbsolutePath()+"\\"+pathline);
									t.start();
								}catch(IOException e)
								{
									e.printStackTrace();
								}
								System.out.println("ARCHIVO ENVIADO");
							}else{
								System.out.println("No se pudo generar la grafica");
								PacketMessage lResponse = new PacketMessage();
								lResponse.message = "REQR|FAIL";
								connection.sendTCP(lResponse);
							}
						}
						
					}
					//cierra los casos de tipos de paquetes
				}
			}
			
		});
		
		System.out.println("Server is operational!");

	}

}
